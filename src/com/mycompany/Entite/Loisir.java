/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entite;

/**
 *
 * @author firas
 */
public class Loisir {
   private String title;


    String adresse;
    int creator;
    String photo;
    String description;

    
    public Loisir() {
    }

    public Loisir(int creator, String title, String description, String adresse,String photo) {
        this.creator = creator;
        this.title = title;
        this.adresse=adresse;
        this.photo=photo;
          this.description=description;
       
    }
public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    @Override
    public String toString() {
        return "Task{" + "creator=" + creator + ", nom=" + title + '}';
    }
           
}
