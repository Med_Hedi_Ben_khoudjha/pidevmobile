/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entite;

/**
 *
 * @author MED
 */
public class offre {
    private int id;
    private String info;
    private int agence_id;
    private String type;
    private String date;
    public offre(int id, String info, int agence_id, String type,String date) {
        this.id = id;
        this.info = info;
        this.agence_id = agence_id;
        this.type = type;
        this.date=date;
    }
    public offre( String type,String info,String date) {
        this.info = info;
        this.type = type;
        this.date=date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    
    public offre() {
    }

    public int getId() {
        return id;
    }

    public String getInfo() {
        return info;
    }

    public int getAgence_id() {
        return agence_id;
    }

    public String getType() {
        return type;
    }

    public void setAgence_id(int agence_id) {
        this.agence_id = agence_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type+"\n"+ info+"\n"+date; 
    }
    
    
}
