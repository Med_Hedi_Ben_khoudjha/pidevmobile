/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entite;


/**
 *
 * @author MED
 */
public class agence {
    private int id;
    private String type;
    private String adresse;
    private String ip;
    private String telephone;
    private String nom;
    private double longitude;
    private double latitude;

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }
    public agence() {
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public agence(int id, String type, String adresse, String ip, String telephone, String nom, double longitude, double latitude) {
        this.id = id;
        this.type = type;
        this.adresse = adresse;
        this.ip = ip;
        this.telephone = telephone;
        this.nom = nom;
        this.longitude = longitude;
        this.latitude = latitude;
    }


public agence(String nom,String type, double latitude, double longitude) {
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.nom = nom;
    }

   
    
    

    public String getAdresse() {
        return adresse;
    }

    public String getIp() {
        return ip;
    }

    public int getId() {
        return id;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getType() {
        return type;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setIp(String avis) {
        this.ip = ip;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setType(String type) {
        this.type = type;
    }

     
    
}
