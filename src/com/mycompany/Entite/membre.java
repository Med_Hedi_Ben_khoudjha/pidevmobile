/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entite;

/**
 *
 * @author MED
 */
public class membre {
    private String nom;
   private String adresse;
private String  email;
private int id;
private boolean motorise;
private boolean permis;
private String prenom;
private String pseudo;
private String psw;
private String role;

    public membre() {
    }

    public membre(String pseudo,String psw,String email) {
this.pseudo=pseudo;
this.psw=psw;
this.email=email;

    }
    
    public String getAdresse() {
        return adresse;
    }

    public String getEmail() {
        return email;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getPsw() {
        return psw;
    }

    public String getRole() {
        return role;
    }

    public boolean isPermis() {
        return permis;
    }

    public boolean isMotorise() {
        return motorise;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMotorise(boolean motorise) {
        this.motorise = motorise;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPermis(boolean permis) {
        this.permis = permis;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setRole(String role) {
        this.role = role;
    }


}
