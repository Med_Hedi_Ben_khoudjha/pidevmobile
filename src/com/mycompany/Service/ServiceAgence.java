/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.mycompany.Entite.agence;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author bhk
 */
public class ServiceAgence {

    public void deleteAgnce(agence p) {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        String Url = "http://localhost/pidevweb/web/app_dev.php/dele_A" + p.getId() ;// création de l'URL
        con.setUrl(Url);// Insertion de l'URL de notre demande de connexion

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
        NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
    }
     
    
    
    
    public void ajoutTask(agence ta) {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        con.setUrl( "http://localhost/pidevweb/web/app_dev.php/newAgence");
        con.setPost(true);

        con.setHttpMethod("POST");
        con.addRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        con.addArgument("type", ta.getType());
        con.addArgument("nom", ta.getNom());
        con.addArgument("latitude",Double.toString( ta.getLatitude()));
        con.addArgument("longitude",Double.toString( ta.getLongitude()));
        
        NetworkManager.getInstance().addToQueueAndWait(con);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
    }

         public ArrayList<agence> getListProd(String json) {

        ArrayList<agence> listUsers = new ArrayList<>();

        try {

            JSONParser j = new JSONParser();

            Map<String, Object> Users = j.parseJSON(new CharArrayReader(json.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) Users.get("root");

            for (Map<String, Object> obj : list) {
                agence user = new agence();

                float id = Float.parseFloat(obj.get("id").toString());
                double latitude = (double) (obj.get("latitude"));
                double longitude = (double) (obj.get("longitude"));

                user.setId((int) id);
                user.setNom(obj.get("nom").toString());
                user.setType(obj.get("type").toString() );
                user.setLongitude((double)longitude);
                user.setLatitude((double)latitude);
                listUsers.add(user);

            }

        } catch (IOException ex) {
        }
        System.out.println(listUsers);
        return listUsers;

    }


    ArrayList<agence> produits = new ArrayList<agence>();

    public ArrayList<agence> getList() {

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pidevweb/web/app_dev.php/agence_all");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceAgence serv = new ServiceAgence();
                produits = serv.getListProd(new String(con.getResponseData()));
            }
        });
        con.setFailSilently(true);
        NetworkManager.getInstance().addToQueueAndWait(con);
        return produits;
    }
}
