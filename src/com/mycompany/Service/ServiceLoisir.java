/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.mycompany.Entite.Loisir;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.MultipartRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.mycompany.Entite.membre;
import com.mycompany.GUI.LoginForm;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Ahmed
 */
public class ServiceLoisir {
    // membre p = login.user;
    public ArrayList<Loisir> affiche(){
          
       ArrayList<Loisir> listTasks = new ArrayList<>();
 ConnectionRequest con = new ConnectionRequest();
       
        con.setUrl("http://127.0.0.1/pidevweb/web/app_dev.php/api/Loisir/all");
           con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {

                //listTasks = getListTask(new String(con.getResponseData()));
             
                JSONParser jsonp = new JSONParser();
                
                try {
                      //renvoi une map avec clé = root et valeur le reste
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                   // System.out.println("roooooot:" +tasks.get("root"));

                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");

                    for (Map<String, Object> obj : list) {
                         
                        Loisir task = new Loisir();
//                        task.setId(((Double) obj.get("id")).intValue());
                         task.setTitle(obj.get("title").toString());
                         task.setDescription(obj.get("description").toString());
                         task.setPhoto(obj.get("photo").toString());
                         task.setAdresse(obj.get("adresse").toString());
        
                         listTasks.add(task);
        
                         
         
                         }
                    
                     } catch (IOException ex) {
                }
  
          
      }
             });
        NetworkManager.getInstance().addToQueueAndWait(con);
       
    return listTasks;
        
                      
      }
        
           public void ajoutLoisir(Loisir ta,MultipartRequest con) {
      // final User me = new User();
           
         System.out.println("Yes");
         //com.mycompany.Entite.User userid=  us.GetUserById(1);      
         String Url = "http://127.0.0.1/pidevweb/web/app_dev.php/api/Loisir/add?title="+ ta.getTitle()+"&photo="+ ta.getPhoto()+"&description="+ ta.getDescription()+"&adresse="+ ta.getAdresse();

//String Url = "http://127.0.0.1/Ecosmartweb/web/app_dev.php/api/annonces/add?titre="+ ta.getTitle()+"&photo="+ ta.getPhoto()+"&createur="+ p.getId()+"&description="+ ta.getDescription()+"&adresse="+ ta.getAdresse();
 //  String Url = "http://127.0.0.1/Ecosmartweb/web/app_dev.php/api/annonces/add?userid="+ me.getId()+"&photo="+ ta.getPhoto()+"&description="+ ta.getDescription()+"&adresse="+ ta.getAdresse();
  
        con.setUrl(Url);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());

        });
        NetworkManager.getInstance().addToQueueAndWait(con);   

    }
        
        
  
    
}
