/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.events.ActionListener;
import com.mycompany.Entite.agence;
import com.mycompany.Entite.membre;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author bhk
 */
public class ServiceMembre {

//    ArrayList<membre> usersLogin = new ArrayList<membre>();
//
//    public ArrayList<membre> getListUsers(String json) {
//
//        ArrayList<membre> listUsers = new ArrayList<>();
//
//        try {
//
//            JSONParser j = new JSONParser();
//
//            Map<String, Object> Users = j.parseJSON(new CharArrayReader(json.toCharArray()));
//
//            List<Map<String, Object>> list = (List<Map<String, Object>>) Users.get("root");
//
//            for (Map<String, Object> obj : list) {
//                membre user = new membre();
//
//                float id = Float.parseFloat(obj.get("id").toString());
//
//                user.setId((int) id);
//                user.setPseudo(obj.get("username").toString());
//                user.setRole(obj.get("roles").toString() );
//                user.setPsw(obj.get("password").toString());
//                user.setEmail(obj.get("email").toString());
//                listUsers.add(user);
//
//            }
//
//        } catch (IOException ex) {
//        }
//        System.out.println(listUsers);
//        return listUsers;
//
//    }
//
//    
//    
//  public ArrayList<membre> getUsers() {
//        ConnectionRequest con = new ConnectionRequest();
//        con.setUrl("http://localhost/pidevweb/web/app_dev.php/login");
//
//        con.addResponseListener(new ActionListener<NetworkEvent>() {
//            @Override
//            public void actionPerformed(NetworkEvent evt) {
//                ServiceMembre usservice = new ServiceMembre();
//
//                usersLogin = usservice.getListUsers(new String(con.getResponseData()));
//
//            }
//        });
//        NetworkManager.getInstance().addToQueueAndWait(con);
//        return usersLogin;
//    }
//
//    
    ArrayList<membre> usersLogin = new ArrayList<membre>();

    public ArrayList<membre> getListUsers(String json) {

        ArrayList<membre> listUsers = new ArrayList<>();

        try {

            JSONParser j = new JSONParser();

            Map<String, Object> Users = j.parseJSON(new CharArrayReader(json.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) Users.get("root");

            for (Map<String, Object> obj : list) {
                membre user = new membre();

                float id = Float.parseFloat(obj.get("id").toString());

                user.setId((int) id);
                user.setPseudo(obj.get("username").toString());
                user.setRole(obj.get("roles").toString() );
                user.setPsw(obj.get("password").toString());
                user.setEmail(obj.get("email").toString());
                listUsers.add(user);

            }

        } catch (IOException ex) {
        }
        System.out.println(listUsers);
        return listUsers;

    }

    
     public void ajoutMenbre(membre ta) {
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        con.setUrl( "http://localhost/pidevweb/web/app_dev.php/newuser");
        con.setPost(true);

        con.setHttpMethod("POST");
        con.addRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        con.addArgument("username", ta.getPseudo());
        con.addArgument("email", ta.getEmail());
        con.addArgument("password",ta.getPsw());
        
        NetworkManager.getInstance().addToQueueAndWait(con);

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
    }

  public ArrayList<membre> getUsers() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pidevweb/web/app_dev.php/login1");

        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceMembre usservice = new ServiceMembre();

                usersLogin = usservice.getListUsers(new String(con.getResponseData()));

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return usersLogin;
    }

    
}
