/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mycompany.GUI;

import com.codename1.components.ToastBar;
import com.codename1.ui.Button;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.agence;
import com.mycompany.Entite.membre;
import com.mycompany.Service.ServiceAgence;
import com.mycompany.Service.ServiceMembre;
import java.util.List;

/**
 * The Login form
 *
 * @author Shai Almog
 */
public class Inscrire extends Form {


    public Inscrire(Resources theme) {

        super(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER_ABSOLUTE));

        setUIID("LoginForm");
        Container welcome = FlowLayout.encloseCenter(
                new Label("S'inscrire ", "WelcomeWhite")        );
        
        getTitleArea().setUIID("Container");
        
       
        
        TextField login = new TextField("", "Adresse e-mail", 20, TextField.EMAILADDR) ;
        TextField pseudo = new TextField("", "Nom d'utilisateur", 20, TextField.ANY) ;
        TextField password = new TextField("", "Mot de passe", 20, TextField.PASSWORD) ;
        TextField password2 = new TextField("", "Répéter le mot de passe", 20, TextField.PASSWORD) ;

        login.getAllStyles().setMargin(LEFT, 0);
        password.getAllStyles().setMargin(LEFT, 0);
        pseudo.getAllStyles().setMargin(LEFT, 0);
        password2.getAllStyles().setMargin(LEFT, 0);
        Label loginIcon = new Label("", "TextField");
        Label loginIcon2 = new Label("", "TextField");
        Label passwordIcon = new Label("", "TextField");
        Label passwordIcon2 = new Label("", "TextField");
        loginIcon.getAllStyles().setMargin(RIGHT, 0);
        loginIcon2.getAllStyles().setMargin(RIGHT, 0);
        passwordIcon.getAllStyles().setMargin(RIGHT, 0);
        passwordIcon2.getAllStyles().setMargin(RIGHT, 0);
        FontImage.setMaterialIcon(loginIcon, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        FontImage.setMaterialIcon(loginIcon2, FontImage.MATERIAL_PERSON_OUTLINE, 3);
        FontImage.setMaterialIcon(passwordIcon, FontImage.MATERIAL_LOCK_OUTLINE, 3);
        FontImage.setMaterialIcon(passwordIcon2, FontImage.MATERIAL_LOCK_OUTLINE, 3);
        
        
           ServiceMembre membre=new ServiceMembre();
            
          
        
                        ServiceMembre y= new ServiceMembre();

        Button loginButton = new Button("S'inscire");
        Button createNewAccount = new Button("Annuler");
        createNewAccount.setUIID("CreateNewAccountButton");
        createNewAccount.addActionListener((evt) -> {
            LoginForm f=new LoginForm(theme);
            f.show();
        });
        loginButton.setUIID("LoginButton");
        loginButton.addActionListener(e -> {
            if(password.getText().equals(password2.getText()))
            {membre m=new membre(pseudo.getText(),password2.getText(),login.getText());
          
                y.ajoutMenbre(m);
               ToastBar.showMessage("Membre Ajouter ", FontImage.MATERIAL_PLACE);

            }
            else{
            ToastBar.showMessage("Verifier mot passe", FontImage.MATERIAL_PLACE);

            }
        });
        
        
        // We remove the extra space for low resolution devices so things fit better
        Label spaceLabel;
        if(!Display.getInstance().isTablet() && Display.getInstance().getDeviceDensity() < Display.DENSITY_VERY_HIGH) {
            spaceLabel = new Label();
        } else {
            spaceLabel = new Label(" ");
        }
        
        
        Container by = BoxLayout.encloseY(
                welcome,
                spaceLabel,
                BorderLayout.center(login).
                        add(BorderLayout.WEST, loginIcon),
                BorderLayout.center(pseudo).
                        add(BorderLayout.WEST, loginIcon2),
                BorderLayout.center(password).
                        add(BorderLayout.WEST, passwordIcon),
                BorderLayout.center(password2).
                        add(BorderLayout.WEST, passwordIcon2),
                
                loginButton,  
                createNewAccount

        );
        add(BorderLayout.CENTER, by);
        
        // for low res and landscape devices
        by.setScrollableY(true);
        by.setScrollVisible(false);
    }
}
