/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mycompany.GUI;

import com.codename1.components.FloatingActionButton;
import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.ToastBar;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.DateFormat;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.payment.Purchase;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.InfiniteContainer;
import com.codename1.ui.Label;
import com.codename1.ui.Slider;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.events.DataChangedListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.agence;
import com.mycompany.Service.ServiceAgence;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a user profile in the app, the first form we open after the walkthru
 *
 * @author Shai Almog
 */
public class ListAgence extends SideMenuBaseForm {
    
    private ArrayList<agence> produits;
    private Container ProduitsList;
     ServiceAgence ser = new ServiceAgence();
    private Image imag;
    private EncodedImage enc;
    private Resources resource;
public static int somme=0;
    
    public ListAgence(Resources res) {
        super(BoxLayout.y());
        resource = res;
        Toolbar tb = getToolbar();
        tb.setTitleCentered(false);
        Image profilePic = res.getImage("user-picture.jpg");
        Image mask = res.getImage("round-mask.png");
        profilePic = profilePic.fill(mask.getWidth(), mask.getHeight());
        Label profilePicLabel = new Label(profilePic, "ProfilePicTitle");
        profilePicLabel.setMask(mask.createMask());

        Button menuButton = new Button("");
        menuButton.setUIID("Title");
        FontImage.setMaterialIcon(menuButton, FontImage.MATERIAL_MENU);
        menuButton.addActionListener(e -> getToolbar().openSideMenu());
        
        Container titleCmp = BoxLayout.encloseY(
                        FlowLayout.encloseIn(menuButton),
                        BorderLayout.centerAbsolute(
                                BoxLayout.encloseY(
                                    new Label(""),
                                    new Label("Shop", "SubTitle")
                                )
                            )
                );
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD_LOCATION);
        tb.setTitleComponent(fab.bindFabToContainer(titleCmp, CENTER, BOTTOM));
            
        Label titre = new Label("Agence : ");
        add(titre);
        add(ChargerListeProduits()); 

           fab.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
            new maps().start();
            }
        });


        
        
        //  add(new Label("Today", "TodayTitle"));
        
        FontImage arrowDown = FontImage.createMaterial(FontImage.MATERIAL_KEYBOARD_ARROW_DOWN, "Label", 3);
        
        
        setupSideMenu(res);
    }
    
    private void addButtonBottom(Image arrowDown, String text, int color, boolean first) {
        MultiButton finishLandingPage = new MultiButton(text);
        finishLandingPage.setEmblem(arrowDown);
        finishLandingPage.setUIID("Container");
        finishLandingPage.setUIIDLine1("TodayEntry");
        finishLandingPage.setIcon(createCircleLine(color, finishLandingPage.getPreferredH(),  first));
        finishLandingPage.setIconUIID("Container");
        add(FlowLayout.encloseIn(finishLandingPage));
    }
    
    private Image createCircleLine(int color, int height, boolean first) {
        Image img = Image.createImage(height, height, 0);
        Graphics g = img.getGraphics();
        g.setAntiAliased(true);
        g.setColor(0xcccccc);
        int y = 0;
        if(first) {
            y = height / 6 + 1;
        }
        g.drawLine(height / 2, y, height / 2, height);
        g.drawLine(height / 2 - 1, y, height / 2 - 1, height);
        g.setColor(color);
        g.fillArc(height / 2 - height / 4, height / 6, height / 2, height / 2, 0, 360);
        return img;
    }

    @Override
    protected void showOtherForm(Resources res) {
        new StatsForm(res).show();
    }
    
    
    private Container ChargerListeProduits()
    {
        ProduitsList = new InfiniteContainer(){
            @Override
            public Component[] fetchComponents(int index, int amount) {
                if (index == 0) {
                    produits = ser.getList();
                }
                if (index + amount > produits.size()) {
                    amount = produits.size() - index;
                }
                if (amount <= 0) {
                    return null;
                }
                Component[] elements = new Component[amount];

                int i = 0;

                
                for (agence e : produits) {

                    //Creating custom container
                    Container element = new Container(BoxLayout.y());
                            String url = "http://localhost/pidevweb/web/app_dev.php/agence_all"+e.getType();
                            try 
                            {
                               // enc = EncodedImage.create("/icon.png");
                            } 
                            catch (Exception ex) 
                            {
                                System.err.println(ex);
                            }
//                            imag = URLImage.createToStorage(enc, url, url, URLImage.RESIZE_SCALE);
                           
                    ImageViewer img = new ImageViewer(imag);
                    Label nameLabel = new Label("Nom  : " + e.getNom());
                    Label type = new Label("Type : "+e.getType()+"");
                    element.add(img);
                    element.add(nameLabel);
                    element.add(type);
                    
                    
                    /* Setting up the connection request */
                    
                    Button delete = new Button("Supprimer ");
                    delete.setUIID("LoginButton");
                    delete.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) 
                        {
                            ser.deleteAgnce( e);
                            Dialog.show("Suppression avec succès","Vouz avez supprimé l'agence"+ e.getNom()+" ","ok",null);
                            new ListAgence(resource).show();
                        }
                    });
                    
                    element.add(delete);
                    elements[i] = element;
                    i++;
                }
                Button delete = new Button("Supprimer de la Wish List");
                
                return elements;
            }
                   
        };
                   
        ProduitsList.setScrollableY(false);
        return  ProduitsList;
    }
    
    
   
}