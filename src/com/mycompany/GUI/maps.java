/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.GUI;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.InteractionDialog;
import com.codename1.components.MultiButton;
import com.codename1.components.ToastBar;
import com.codename1.googlemaps.MapContainer;
import com.codename1.googlemaps.MapContainer.MapObject;
import com.codename1.io.Log;
import com.codename1.io.Util;
import com.codename1.maps.BoundingBox;
import com.codename1.maps.Coord;
import com.codename1.maps.MapListener;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import static com.codename1.ui.ComponentSelector.$;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.SideMenuBar;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Rectangle;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.list.GenericListCellRenderer;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.agence;
import com.mycompany.Entite.membre;
import com.mycompany.Service.ServiceAgence;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author MED
 */
public class maps extends Form{
private static final String HTML_API_KEY = "AIzaSyBWeRU02YUYPdwRuMFyTKIXUbHjq6e35Gw";
    private Form current;
    
    
      double lan=0;
      double log=0;
        Coord c=null;
                ServiceAgence y= new ServiceAgence();

         agence n=null;

    public void init(Object context) {
        try {
            
  
        
            Resources theme = Resources.openLayered("/theme_1");
            UIManager.getInstance().setThemeProps(theme.getTheme(theme.getThemeResourceNames()[0]));
            Display.getInstance().setCommandBehavior(Display.COMMAND_BEHAVIOR_SIDE_NAVIGATION);
            UIManager.getInstance().getLookAndFeel().setMenuBarClass(SideMenuBar.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    MapObject sydney;
    public void start() {
        if (current != null) {
            current.show();
            return;
        }
        Form hi = new Form("Agence");
        hi.setLayout(new BorderLayout());
         MapContainer cnt = new MapContainer(HTML_API_KEY);
        ServiceAgence agence=new ServiceAgence();
            List l=agence.getList();
        //final MapContainer cnt = new MapContainer();
        cnt.setCameraPosition(new Coord(36.476660, 9.450901));//this breaks the code //because the Google map is not loaded yet
        cnt.addMapListener(new MapListener() {

            @Override
            public void mapPositionUpdated(Component source, int zoom, Coord center) {
               lan=0;
               log=0;
                System.out.println("Map position updated: zoom="+zoom+", Center="+center);
                lan=center.getLatitude();
                log=center.getLongitude();
            }
            
        });
  lan=0;
               log=0;


Style s = new Style();
        s.setFgColor(0xff0000);
        s.setBgTransparency(0);
        FontImage markerImg = FontImage.createMaterial(FontImage.MATERIAL_PLACE, s, 3);
     for(int i=0;i<l.size();i++)
     { n=new agence();
                n=(agence) l.get(i);
                System.out.println(n.getLatitude());
             c=new Coord(n.getLatitude(), n.getLongitude());
            cnt.addMarker(null, c, n.getNom()+" "+n.getType(), n.getType()+" "+n.getNom(), null );
                

     }

        
      
        cnt.addTapListener(e->{
            if (tapDisabled) {
                return;
            }
            tapDisabled = true;
            TextField enterName = new TextField();
            Button add=new Button("Ajouter");
            Button Annuler=new Button("Annuler");
 ComboBox combo = new ComboBox(); 
 combo.addItem("Restaurent");
 combo.addItem("Hotel");
 combo.addItem("Café");
         
  
            Container wrapper = BoxLayout.encloseY(new Label("Name:"), enterName,combo,add,Annuler);
            InteractionDialog dlg = new InteractionDialog("Add Marker");
            dlg.getContentPane().add(wrapper);
            Annuler.addActionListener((evt) -> {
                dlg.dispose();
                tapDisabled = false;
            });
            add.addActionListener(e2->{
                agence m=new agence(enterName.getText().toString(),
                        combo.getSelectedItem().toString(),
                        lan, log);
                y.ajoutTask(m);
                lan=0;
                log=0;
                 cnt.addMarker(EncodedImage.createFromImage(markerImg, false), cnt.getCoordAtPosition(e.getX(), e.getY()), enterName.getText(), "", e3->{
                    ToastBar.showMessage("You clicked "+m.getNom()+" "+m.getType(), FontImage.MATERIAL_PLACE);
                });
                
                
                dlg.dispose();
                ToastBar.showMessage("Agence ajouter avec succées.",FontImage.MATERIAL_DONE);

                tapDisabled = false;
            });
            dlg.showPopupDialog(new Rectangle(e.getX(), e.getY(), 20, 20));
            enterName.startEditingAsync();
        });  
        
        
       
                Button annuler = new Button("Annuler");
annuler.addActionListener((evt) -> {
    this.stop();
});
        
        
        
        
       
        FloatingActionButton nextForm = FloatingActionButton.createFAB(FontImage.MATERIAL_ACCESS_ALARM);
  
        
        
        Container root = LayeredLayout.encloseIn(
                BorderLayout.center(nextForm.bindFabToContainer(cnt)),BorderLayout.south(
                        FlowLayout.encloseBottom(annuler )
                )
                              
        );
        
        hi.add(BorderLayout.CENTER, root);
        hi.show();
        
    }
    boolean tapDisabled = false;

   
    
    public void stop() {
        current = Display.getInstance().getCurrent();
    }

    public void destroy() {
    }

    

    }
    

// <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    
